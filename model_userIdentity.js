"use strict";
var mongoose = require("mongoose");

var userIdentitySchema = mongoose.Schema({
    fname: {
        type: String
    },
    lname: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    userId: {
        type: String,
        unique: true
    },
    status: {
        type: String,
        enum: ["setPasswordHint","setSecurityQuestions","2faRequired","forceLogout","passwordExpired","authenticated"]
    },
    pan: {
        type: String
    },
    mobile: {
        type: Number
    },
    password: {
        type: String
    },
    iamSystem: {
        type: String,
        default: "Mongo"
    },
    attempts: {
        type: Number,
        default: 0
    },
    oldPasswords: {
        type: Array
    },
    ecipherType: {
        type: String,
        enum: ["securityQuestions","emailMobilePan","none"],
        default:"none"
    },
    isLocked: {
        type: Boolean,
        default:false
    },
    expires: {
        type: Date
    },
    passwordUpdated: {
        type: Date,
        default: new Date()
    },
    securityQuestions: [{
        question: {
            type: String
        },
        answer: {
            type: String
        }
    }],
    securityQuestionsSetUp:{
        type: Boolean,
        default:true
    },
    securityQuestionsUpdated: {
        type: Date,
        default: new Date()
    },
    passwordHint: {
        type: String
    },
    passwordHintSetUp:{
        type: Boolean,
        default:true
    },
    passwordTemplateId:{
        type: String
    },
    eciperTemplateId:{
        type: String
    },
    pwdSts:{
        type: String,
        enum: ["locked","active"],
        default:"active"
    },
    clSts:{
        type: String,
        enum: ["inActive","active","suspended","dorment"],
        default:"active"
    },
    DOB: {
        type: Date,
        default: new Date()
    },
    lastLoggedIn: {
        type: Date
    }
});
    
userIdentitySchema.methods.toJson = function () {
    var userInfo = this.toObject();
    delete userInfo.password;
    return userInfo;
};


module.exports = mongoose.model("UserIdentity", userIdentitySchema);
var csv = require('ya-csv');
var mongoose = require('mongoose');
var UserIdentity = require("./model_userIdentity");
var reader = csv.createCsvFileReader('UserScheme_1.csv', { columnsFromHeader: true });
var user;
var document = {};
var userArray =[];
var finalData = [];
mongoose.connect("mongodb://localhost/religare", { server: { reconnectTries: 9999, reconnectInterval: 4000 } }, function(err){
	if(err)
		console.log(err);
	else
	console.log("Connected to DB : ");
}); 
reader.addListener('data', function(data) {
    document = {};
    document.clientCode = data.ClientCode;
    document.fname = data.clientName;
    document.email = "religare@religare.com";
    document.pan = data.PANNumber;
    document.mobile = 9999999999;
    document.passwordTemplateId = "586a489e7cd042428889c926";
    document.eciperTemplateId = "586a489e7cd042428889c926";
    document.password = "$2a$07$yydt3rmhMRVCPMC7.oSyTeZrCpty8ZEpfR0DVv4TTWfbBQz0ohRQq";
    document.DOB = "2017-01-10T11:19:03.539Z";
    document.clSts = "active";
    document.authSts = "unLocked";
    document.passwordHintSetUpRequired = false;
    document.securityQuestionsUpdated = "2017-01-11T18:25:08.163Z";
    document.securityQuestionsSetUpRequired = false;
    document.securityQuestions = [];
    document.passwordUpdated = "2016-01-11T18:25:08.163Z";
    document.oldPasswords = [];
    document.eCiperAttempts = 0;
    document.passwordAttempts = 0;
    document.iamSystem = "Mongo";
    document.passwordHint = "hiiiiii";
    document.forgotPasswordToken = false;
    userArray.push(document);
});
reader.addListener('end', function(data){
    // console.log(mongoose.connection.collection('customer').initializeUnorderedBulkOp);
     console.log("userArray.length",userArray.length);
     var bulk = mongoose.connection.collection('users').initializeUnorderedBulkOp();
     userArray.forEach(function(user){
         bulk.insert(user);
     });

     bulk.execute(function (err, result) {
        if (err) {
            console.log(err);
        } else {
            console.log("inserted");
        }
    });   
});

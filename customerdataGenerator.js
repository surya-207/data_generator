var csv = require('ya-csv');
var count = 0;
var mongoose = require('mongoose');
var Customer = require("./model_customer");
var UserIdentity = require("./model_userIdentity");
var reader = csv.createCsvFileReader('UserScheme_1.csv', { columnsFromHeader: true });
var customer;
var document = {};
var customerArray =[];
var finalData = [];
mongoose.connect("mongodb://localhost/religare", { server: { reconnectTries: 9999, reconnectInterval: 4000 } }, function(err){
	if(err)
		console.log(err);
	else
	console.log("Connected to DB : ");
}); 
reader.addListener('data', function(data) {
    document = {};
    document.clientCode = data.ClientCode;
    document.frstName = data.clientName;
    document.PAN = data.PANNumber;
    document.clType = data.clientType;
    document.custCtgry = data.customerCategory;
    document.prmryDlr = data.primaryDealer;
    document.eveningDlr = data.eveningDealer;
    document.branchCode = data.branchCode;
    document.groupID = data.groupID;
    document.branchID = data.branchID;
    document.dealerID = data.dealerID;
    document.clSts = data.clientStatus;
    document.tradeSeg = data.tradingSegments.substring(1, data.tradingSegments.length-1).split(",");
    document.bcastSeg = data.bcastSegments.substring(1, data.bcastSegments.length-1).split(",");
    document.loginSeg = data.loginSegments.substring(1, data.loginSegments.length-1).split(",");
    document.maxBcastScrips = data.maxBcastScrips;
    document.ordrPerSec = data.orderPerSec;
    document.branchName = data.branchName;
    document.usrCreatnTime = data.userCreationTime;
    document.partType = data.participantType;
    document.connMode = data.connectionMode.substring(1, data.connectionMode.length-1).split(",");;
    document.productsAllowed = data.productsAllowed;
    document.pwdSts = data.pwdStatus;
    document.brokerageTmplt = data.brokerageTemplate;
    document.DMAAllowed = data.DMAAllowed;
    document.investmntProd = data.investmentProducts.substring(1, data.investmentProducts.length-1).split(",");
    document.DPAccType = data.DPAccountType;
    document.DPIDNo = data.DPIDNo;
    document.ARNNo = data.ARNNumber;
    document.riskProfTmplt = data.riskProfile;
    document.rmsEnabled = data.RMSENabled;
    document.bannedScripTmplt = data.bannedScripTemplate;
    document.roleTmpltID = data.RoleTemplateID;
    document.squareOffTmplt = data.SquareOffTemplate;
    customerArray.push(document);
});
reader.addListener('end', function(data){
     var bulk = mongoose.connection.collection('cust').initializeUnorderedBulkOp();
     customerArray.forEach(function(customer){
         bulk.insert(customer);
     });
     bulk.execute(function (err, result) {
        if (err) {
            console.log(err);
        } else {
            console.log("inserted");
        }
    });   
});
